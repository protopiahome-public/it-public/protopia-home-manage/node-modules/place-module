// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'place';

module.exports = {

  Mutation: {
    changePlace: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args._id) {
        return await query(collectionItemActor, { type: 'place', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'place', input: args.input }, global.actor_timeout);
    },
  },
  Query: {

    getPlace: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'place', search: { _id: args._id } }, global.actor_timeout);
    },

    getPlaces: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'place' }, global.actor_timeout);
    },
  },

};
